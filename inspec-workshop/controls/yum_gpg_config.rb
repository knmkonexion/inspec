control "Yum GPG Config" do
  impact 1.0
  title "Verify that gpgcheck is Globally Activated in yum (Scored)"
  desc "The gpgcheck option, found in the main section of the /etc/yum.conf file determines if an RPM package's signature is always checked prior to its installation."
  describe file('/etc/yum.conf') do
    its('content') { should match /gpgcheck=1/ }
  end
end
